// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to HEX conversions", () => {
        it("coverts the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // #ff0000
            expect(redHex).to.equal("#ff0000"); //red hex value

            const blueHex = converter.rgbToHex(0, 0, 255);
            expect(blueHex).to.equal("#0000ff");
        });
    });
});